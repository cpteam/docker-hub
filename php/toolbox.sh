#!/usr/bin/env bash


if [ $1 = "composer" ]; then
	echo "Install composer ..."

	EXPECTED_SIGNATURE=$(wget -q -O - https://composer.github.io/installer.sig)
	php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
	ACTUAL_SIGNATURE=$(php -r "echo hash_file('SHA384', 'composer-setup.php');")

	if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]
	then
		>&2 echo 'ERROR: Invalid installer signature'
		rm composer-setup.php
		exit 1
	fi

	php composer-setup.php --quiet

	mv composer.phar /usr/local/bin/composer
	chmod +x /usr/local/bin/composer
	#composer global require hirak/prestissimo --no-interaction -vvv

	RESULT=$?
	rm composer-setup.php
	exit $RESULT
fi


if [ $1 = "xdebug" ]; then
	echo "Install xDebug ..."
	yes | pecl install xdebug \
		&& echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > /usr/local/etc/php/conf.d/xdebug.ini \
		&& echo "xdebug.remote_enable=on" >> /usr/local/etc/php/conf.d/xdebug.ini \
		&& echo "xdebug.remote_autostart=off" >> /usr/local/etc/php/conf.d/xdebug.ini

	exit 0
fi

if [ $1 = "debugMode" ]; then
	PATH=$2
	DEBUG_MODE=$3

	echo "<?php" > $PATH

	if [ $DEBUG_MODE = "1" ]; then
		echo "DEBUG mode ENABLED"
		echo "return true;" >> $PATH
	else
		echo "DEBUG mode DISABLED"
		echo "return false;" >> $PATH
	fi

	exit 0
fi
