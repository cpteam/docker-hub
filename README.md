# K cemu tento repozitar slouzi

* v tomto repozitari se nachazi skripty a nastaveni pro build containeru, ktere jsou shipnuty na hub.docker.com (do verejnych repozitaru) 
* napriklad webgarden/php
* tyto containery jsou pak pouzivany napric aplikacema
* ve vychozim stavu obsahuji zakladni funkce, ktere z velke vetsiny pouzivame

## Proc se containery buildi takto
* diky predbuildeni klicovych feature se mnohonasobne snizuje cas buildu nutny na kazdem nasem pocitaci 
* je zde pouzita dedicnost

## PHP containery obsahuji
* redis
* xdebug
* composer

* Proces je zautomatizovany, takze na zacatku kazdeho mesice dostaneme do projektu posledni path verzi PHPka (7.1.x) 
