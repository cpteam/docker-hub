#!/usr/bin/env bash

PHP=cpteam/php

DOCKER_HUB_USER=$1
DOCKER_HUB_PSW=$2
shift 2

if [ -e config.sh ]; then
	source config.sh
fi


buildPhp() {
	VERSION=$1
	HUB_VERSION=$1-fpm
	shift

	if [[ $HUB_VERSION = "7.2-fpm" ]]; then
		HUB_VERSION="7.2.0RC1-fpm"
	fi

	echo "Build PHP $VERSION with XDebug & Redis"
	docker build php ${BUILD_OPT} \
		--build-arg FROM=php:$HUB_VERSION \
		--tag $PHP:$VERSION \
		$*
}

## Build containers
#buildPhp 7.0
buildPhp 7.1 \
	--tag $PHP:7 \
	--tag $PHP:latest

## Push all containers
docker login -u $DOCKER_HUB_USER -p $DOCKER_HUB_PSW
docker push $PHP
docker logout

## check all containers

check() {
	echo
	echo "- [ $1 DEFAULT ] -----------------------------------"
	echo
	docker run $PHP:$1 php -v
	echo
	docker run $PHP:$1 php -m | grep redis
	echo
	docker run $PHP:$1 composer --version
	docker run $PHP:$1 unzip --help
	echo

}

#check 7.0
check 7.1
#check 7.2
